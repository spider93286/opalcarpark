from flask import request, render_template, redirect, url_for, Flask, jsonify
import requests, json
import requests_cache
from app import app

session = requests_cache.CachedSession('opal_cache')

@app.route("/", methods=['GET', 'POST'])
def index():
    return render_template("index.html")

@app.route("/getCarparks", methods=['GET'])
def getCarparks():
    api_key = "Y2c69i8Cmf6QA5g838QzNSXPzbGEYssJDySK"
    base_url = "https://api.transport.nsw.gov.au/v1/carpark"
    headers = {"Authorization": "apikey " + api_key}
    response = session.get(base_url, headers=headers, stream=True)
    data = json.loads(response.content)
    print(data)
    return jsonify(data)

@app.route('/getCarparks/<id>', methods=['GET'])
def getCarParkInfo(id):
    api_key = "Y2c69i8Cmf6QA5g838QzNSXPzbGEYssJDySK"
    base_url = "https://api.transport.nsw.gov.au/v1/carpark"
    headers = {"Authorization": "apikey " + api_key}
    payload = {"facility": id}
    response = session.get(base_url, headers=headers, stream=True, params=payload)
    data = json.loads(response.content)
    return jsonify(data)
