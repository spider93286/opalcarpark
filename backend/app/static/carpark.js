$(document).ready(function () {

    $.ajax({
        url: "/getCarparks",
        type: "GET",
        dataType: "json",
        success: async function (result) {
            $('#loading').css('display', 'none');
            for (const key of Object.keys(result)) {
                await getCarparkInfo(key);
            }
        }
    });

    async function getCarparkInfo(key) {
        await $.ajax({
            url: "/getCarparks/" + key.toString(),
            type: 'GET',
            dataType: "json",
            success: function (result) {
                //console.log(result)
                //console.log('date', moment(result['MessageDate']));
                var formattedDate = moment(result['MessageDate']).format('DD/MM/YY h:mm a');
                var name = result['facility_name'];
                var spotsOccupied = result['occupancy']['total'];
                var totalSpots = result['spots'];
                var percentFull = ((spotsOccupied/totalSpots) * 100).toFixed(0);
                //console.log(percentFull.toString());
                var htmlToAppend = '';
                htmlToAppend += '<div class="carpark">';
                htmlToAppend += '<div id="top">';
                htmlToAppend += '<h3>' + name + '</h3>';
                htmlToAppend += '<p>Updated ' + formattedDate + '</p>';
                htmlToAppend += '</div>';
                
                htmlToAppend += '<h6>' + spotsOccupied.toString() + '/' + totalSpots.toString() + ' occupied</h6>';
                htmlToAppend += '<div class="w3-border">';
                htmlToAppend += '<div class="w3-green w3-center" style="height:24px;width:' + percentFull + '%">' +
                    percentFull.toString() + '%' + '</div>'
                htmlToAppend += '</div>';
                htmlToAppend += '</div>';
                //console.log(moment.locale());
                $('body').append(htmlToAppend);

                $('.carpark').sort(function (a, b) {
                    var firstText = a.firstChild.firstChild.innerHTML;
                    var secondText = b.firstChild.firstChild.innerHTML;
                    return firstText.localeCompare(secondText);
                }).appendTo('body')
            }
        })
    }
});

